package androidx.viewpager2.integration.testapp

import kotlinx.coroutines.channels.ticker

object TickerUtils {
    suspend fun startTicker(
        delayMillis: Long = 1000,
        initialDelayMillis: Long = 0,
        block: suspend () -> Unit
    ) {
        val ticker = ticker(delayMillis, initialDelayMillis)
        for (event in ticker) {
            block()
        }
    }
}