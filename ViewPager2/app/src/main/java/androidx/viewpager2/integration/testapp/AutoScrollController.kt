/*
 * Copyright 2019 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package androidx.viewpager2.integration.testapp

import android.os.Build
import android.view.View
import android.view.ViewTreeObserver
import android.widget.CheckBox
import androidx.annotation.RequiresApi
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.widget.ViewPager2
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class AutoScrollController(
    private val viewPager: ViewPager2,
    private val checkBox: CheckBox,
    private val isReversal: Boolean = false
) {
    var tickerJob: Job? = null

    private fun isActive() = tickerJob != null && tickerJob?.isCancelled == false

    fun setUp() {
        fetchDragDistance(viewPager)

        checkBox.isChecked = isActive()
        checkBox.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                startTicker(true)
            } else {
                stopTicker()
            }
        }
    }

    fun bindToLifecycle(lifecycle: Lifecycle) {
        // OnResume
        if (checkBox.isChecked) {
            startTicker(false)
        }
        // OnPause
        stopTicker()
    }

    private fun startTicker(reset: Boolean = false) {
        if (reset) {
            stopTicker()
        }

        if (tickerJob == null || tickerJob?.isCancelled == true) {
            tickerJob = CoroutineScope(Dispatchers.IO).launch {
                TickerUtils.startTicker(1500, 0) {
                    if (viewPager.scrollState == ViewPager2.SCROLL_STATE_IDLE) {
                        if (isReversal) {
                            val next = viewPager.currentItem - 1
                            if (next < 0) {
                                gotoLatestPosition()
                            } else {
                                //gotoPosition(next)
                                beginDrag()
                            }
                        } else {
                            val next = viewPager.currentItem + 1
                            if (next == viewPager.adapter?.itemCount) {
                                gotoStartPosition()
                            } else {
                                //gotoPosition(next)
                                beginDrag()
                            }
                        }
                    }
                }
            }
        }
    }

    private fun stopTicker() {
        if (tickerJob?.isCancelled == false) {
            tickerJob?.cancel()
        }
    }

    private fun gotoStartPosition() {
        viewPager.post {
            viewPager.setCurrentItem(0, false)
        }
    }

    private fun gotoLatestPosition() {
        viewPager.adapter?.itemCount?.takeIf { it > 0 }?.also {
            viewPager.post {
                viewPager.setCurrentItem(it - 1, false)
            }
        }
    }

    // Fast
    private fun gotoPosition(position: Int) {
        viewPager.post {
            viewPager.setCurrentItem(position, true)
        }
    }

    private fun fetchDragDistance(view: View) {
        if (view.viewTreeObserver.isAlive) {
            view.viewTreeObserver.addOnGlobalLayoutListener(object :
                ViewTreeObserver.OnGlobalLayoutListener {
                @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
                override fun onGlobalLayout() {
                    setDragDistance(view.width.toFloat())
                    view.viewTreeObserver.removeOnGlobalLayoutListener(this)
                }
            })
        }
    }

    private val frames = 60
    private val duration = 1000L
    private val delay = duration / frames
    private var dragDistance = 0f
    private var perMove = 0f

    private fun setDragDistance(d: Float) {
        dragDistance = d
        perMove = d / frames.toFloat() * (if (isReversal) 1 else -1)
    }

    private fun beginDrag() {
        if (dragDistance > 0f) {
            if (delay > 30) {
                viewPager.post {
                    viewPager.beginFakeDrag()
                    normalDragTo(dragDistance)
                }
            } else {
                fastDragTo()
            }
        }
    }

    private fun normalDragTo(to: Float) {
        viewPager.fakeDragBy(perMove)

        val cost = when {
            perMove < 0 -> -perMove
            else -> perMove
        }

        val result = to - cost
        if (result < 1) {
            viewPager.endFakeDrag()
        } else {
            viewPager.postDelayed({
                normalDragTo(result)
            }, delay)
        }
    }

    private fun fastDragTo() {
        if (frames > 0) {
            viewPager.post {
                viewPager.beginFakeDrag()
            }

            repeat(frames) { i ->
                if (i == frames - 1) {
                    viewPager.postDelayed({
                        viewPager.endFakeDrag()
                    }, i * delay)
                } else {
                    viewPager.postDelayed({
                        viewPager.fakeDragBy(perMove)
                    }, i * delay)
                }
            }
        }
    }
}