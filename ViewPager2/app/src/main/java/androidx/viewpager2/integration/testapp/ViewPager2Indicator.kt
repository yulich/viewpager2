package androidx.viewpager2.integration.testapp

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import androidx.viewpager2.widget.ViewPager2
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback

class ViewPager2Indicator(context: Context, attrs: AttributeSet) : View(context, attrs) {
    companion object {
        /**
         * 計算繪製圓形貝塞爾曲線控製點的位置
         */
        private const val M = 0.551915024494f

        fun getIndicatorTypeBy(index: Int) = IndicatorType.values()[index]
    }

    /**
     * 線, 圓, 圓線, 貝塞爾, 彈性球, 進度條
     */
    enum class IndicatorType {
        LINE, CIRCLE, CIRCLE_LINE, BEZIER, SPRING, PROGRESS
    }

    /**
     * 佈局, 距離, 半徑
     */
    enum class DistanceType {
        BY_RADIUS, BY_DISTANCE, BY_LAYOUT
    }

    init {
        setDefaultStyleable(context, attrs)
    }

    private lateinit var mFillPaint: Paint
    private lateinit var mStrokePaint: Paint
    private var mDistanceType = DistanceType.BY_RADIUS //距離類型
    private var mIndicatorType = IndicatorType.LINE //點類型
    private var mCycleNumber = 0 //個數
    private var mRadius = 0f //半徑
    private var mRadiusSelected = 0f //選中半徑，默認為mRadius
    private var mLength = 0f //線長
    private var mOffset = 0f //偏移量
    private var mSelectedColor = 0 //選中顏色
    private var mDefaultColor = 0 //默認顏色
    private var mDistance = 0f //間隔距離
    private var mPosition = 0//第幾張
    private var mPercent = 0f
    private var mIsLeft = false
    private var mAnimation = false
    private var mPath = Path()
    private val mRectClose = RectF()
    private val mRectOpen = RectF()

    private fun setDefaultStyleable(context: Context, attrs: AttributeSet) {
        mSelectedColor = Color.BLACK
        mDefaultColor = Color.parseColor("#ffcdcdcd")
        mRadius = 12f
        mRadiusSelected = mRadius
        mLength = 2 * mRadius
        mDistance = 3 * mRadius
        mDistanceType = DistanceType.BY_RADIUS
        mIndicatorType = IndicatorType.LINE
        mCycleNumber = 0
        mAnimation = true

        initPaint()

        invalidate()
    }

    private fun initPaint() {
        mStrokePaint = Paint().apply {
            //空心
            style = Paint.Style.FILL
            color = mDefaultColor
            isAntiAlias = true
            strokeWidth = 3f
        }
        mFillPaint = Paint().apply {
            style = Paint.Style.FILL_AND_STROKE
            color = mSelectedColor
            isAntiAlias = true
            strokeWidth = 3f
        }
    }

    /**
     * invalidate()後執行
     *
     * @param canvas
     */
    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        if (mCycleNumber <= 0) {
            return
        }

        canvas.translate(width / 2f, height / 2f)

        when (mDistanceType) {
            DistanceType.BY_DISTANCE -> {
            }
            DistanceType.BY_RADIUS -> mDistance = 3 * mRadius
            DistanceType.BY_LAYOUT -> mDistance = if (mIndicatorType == IndicatorType.CIRCLE_LINE) {
                width / (mCycleNumber + 1).toFloat()
            } else {
                width / mCycleNumber.toFloat()
            }
        }
        when (mIndicatorType) {
            IndicatorType.CIRCLE -> {
                var i = 0
                while (i < mCycleNumber) {
                    //默認點 -(mNum - 1) * .5f * mDistance 第一個點
                    canvas.drawCircle(
                        -(mCycleNumber - 1) * .5f * mDistance + i * mDistance,
                        0f,
                        mRadius,
                        mStrokePaint
                    )
                    i++
                }
                //選中
                canvas.drawCircle(
                    -(mCycleNumber - 1) * .5f * mDistance + mOffset,
                    0f,
                    mRadiusSelected,
                    mFillPaint
                )
            }
            IndicatorType.LINE -> {
                mStrokePaint.strokeWidth = mRadius
                val startX = -(mCycleNumber - 1) * .5f * mDistance - mLength / 2
                val stopX = -(mCycleNumber - 1) * .5f * mDistance + mLength / 2
                //默認
                var i = 0
                while (i < mCycleNumber) {
                    canvas.drawLine(
                        startX + i * mDistance,
                        0f,
                        stopX + i * mDistance,
                        0f,
                        mStrokePaint
                    )
                    i++
                }
                //選中
                mFillPaint.strokeWidth = mRadius
                val startF = -(mCycleNumber - 1) * .5f * mDistance - mLength / 2 + mOffset
                val stopF = -(mCycleNumber - 1) * .5f * mDistance + mLength / 2 + mOffset
                canvas.drawLine(startF, 0f, stopF, 0f, mFillPaint)
            }
            IndicatorType.CIRCLE_LINE -> if (mPosition == mCycleNumber - 1) { //最後一個 右滑
                //第一個 線 選中 消失
                val leftClose = -mCycleNumber * .5f * mDistance - mRadius
                val rightClose = leftClose + 2 * mRadius + mOffset
                val topClose = -mRadius
                val bottomClose = mRadius
                mRectClose.set(leftClose, topClose, rightClose, bottomClose) // 設置個新的長方形
                canvas.drawRoundRect(mRectClose, mRadius, mRadius, mStrokePaint)
                //最後一個 線  顯示
                val rightOpen =
                    -mCycleNumber * .5f * mDistance + mCycleNumber * mDistance + mRadius
                val leftOpen = rightOpen - 2 * mRadius - mDistance + mOffset
                val topOpen = -mRadius
                val bottomOpen = mRadius
                mRectOpen.set(leftOpen, topOpen, rightOpen, bottomOpen) // 設置個新的長方形
                canvas.drawRoundRect(mRectOpen, mRadius, mRadius, mStrokePaint)
                //圓
                var i = 1
                while (i < mCycleNumber) {
                    canvas.drawCircle(
                        rightClose - mRadius + i * mDistance,
                        0f,
                        mRadius,
                        mStrokePaint
                    )
                    i++
                }
            } else {
                //第一個 線 選中 消失
                val leftClose = -mCycleNumber * .5f * mDistance + mPosition * mDistance - mRadius
                val rightClose = leftClose + 2 * mRadius + mDistance - mOffset
                val topClose = -mRadius
                val bottomClose = mRadius
                mRectClose.set(leftClose, topClose, rightClose, bottomClose) // 設置個新的長方形
                canvas.drawRoundRect(mRectClose, mRadius, mRadius, mStrokePaint)
                //第二個 線  顯示
                if (mPosition < mCycleNumber - 1) {
                    val rightOpen =
                        -mCycleNumber * .5f * mDistance + (mPosition + 2) * mDistance + mRadius
                    val leftOpen = rightOpen - 2 * mRadius - mOffset
                    val topOpen = -mRadius
                    val bottomOpen = mRadius
                    mRectOpen.set(leftOpen, topOpen, rightOpen, bottomOpen) // 設置個新的長方形
                    canvas.drawRoundRect(mRectOpen, mRadius, mRadius, mStrokePaint)
                }
                //圓
                var i = mPosition + 3
                while (i <= mCycleNumber) {
                    canvas.drawCircle(
                        -mCycleNumber * .5f * mDistance + i * mDistance,
                        0f,
                        mRadius,
                        mStrokePaint
                    )
                    i++
                }

                i = mPosition - 1
                while (i >= 0) {
                    canvas.drawCircle(
                        -mCycleNumber * .5f * mDistance + i * mDistance,
                        0f,
                        mRadius,
                        mStrokePaint
                    )
                    i--
                }
            }
            IndicatorType.BEZIER -> {
                var i = 0
                while (i < mCycleNumber) {
                    //默認點 -(mNum - 1) * .5f * mDistance 第一個點
                    canvas.drawCircle(
                        -(mCycleNumber - 1) * .5f * mDistance + i * mDistance,
                        0f,
                        mRadius,
                        mStrokePaint
                    )
                    i++
                }
                //選中
                drawCubicBezier(canvas)
            }
            IndicatorType.SPRING -> {
                var i = 0
                while (i < mCycleNumber) {
                    //默認點 -(mNum - 1) * .5f * mDistance 第一個點
                    canvas.drawCircle(
                        -(mCycleNumber - 1) * .5f * mDistance + i * mDistance,
                        0f,
                        mRadius,
                        mStrokePaint
                    )
                    i++
                }
                drawSpringBezier(canvas)
            }
            IndicatorType.PROGRESS -> {
                var i = 0
                while (i < mCycleNumber) {
                    //默認點 -(mNum - 1) * .5f * mDistance 第一個點
                    canvas.drawCircle(
                        -(mCycleNumber - 1) * .5f * mDistance + i * mDistance,
                        0f,
                        mRadius,
                        mStrokePaint
                    )
                    i++
                }
                //選中
                val rightOpen = -(mCycleNumber - 1) * .5f * mDistance + mOffset + mRadius
                val leftOpen = -(mCycleNumber - 1) * .5f * mDistance - mRadius
                val topOpen = -mRadius
                val bottomOpen = mRadius
                mRectOpen.set(leftOpen, topOpen, rightOpen, bottomOpen) // 設置個新的長方形
                canvas.drawRoundRect(mRectOpen, mRadius, mRadius, mFillPaint)
            }
        }
    }

    private val mSpringPoint by lazy {
        arrayOf(Point(), Point(), Point(), Point(), Point(), Point())
    }

    /**
     * 繪製彈性
     *
     * @param canvas
     */
    private fun drawSpringBezier(canvas: Canvas) {
        //右圓圓心
        val rightCircleX: Float
        //右圓半徑
        val rightCircleRadius: Float
        //左圓圓心
        val leftCircleX: Float
        //左圓半徑
        val leftCircleRadius: Float
        //最大半徑
        val maxRadius = mRadius
        //最小半徑
        val minRadius = mRadius / 2
        //控製點
        if (mPosition == mCycleNumber - 1 && !mIsLeft) { //第一個 右滑  0---4
            if (mPercent <= 0.5) {
                rightCircleX =
                    -(mCycleNumber - 1) * .5f * mDistance + (mCycleNumber - 1) * mDistance
                leftCircleX =
                    -(mCycleNumber - 1) * .5f * mDistance + (.5f - mPercent) / .5f * (mCycleNumber - 1) * mDistance
                rightCircleRadius =
                    minRadius + (maxRadius - minRadius) * (.5f - mPercent) / .5f
            } else {
                rightCircleX =
                    -(mCycleNumber - 1) * .5f * mDistance + (1f - mPercent) / .5f * (mCycleNumber - 1) * mDistance
                leftCircleX = -(mCycleNumber - 1) * .5f * mDistance
                rightCircleRadius = minRadius
            }
            leftCircleRadius = mRadius * mPercent
        } else if (mPosition == mCycleNumber - 1 && mIsLeft) { //最後一個 左滑 4--0
            if (mPercent >= 0.5) { //左亭
                leftCircleRadius =
                    minRadius + (maxRadius - minRadius) * (-.5f + mPercent) / .5f
                leftCircleX = -(mCycleNumber - 1) * .5f * mDistance
                rightCircleX =
                    -(mCycleNumber - 1) * .5f * mDistance + (1 - mPercent) / .5f * (mCycleNumber - 1) * mDistance
            } else { //左動
                leftCircleRadius = minRadius
                leftCircleX =
                    -(mCycleNumber - 1) * .5f * mDistance + (.5f - mPercent) / .5f * (mCycleNumber - 1) * mDistance
                rightCircleX =
                    -(mCycleNumber - 1) * .5f * mDistance + (mCycleNumber - 1) * mDistance
            }
            rightCircleRadius = mRadius * (1 - mPercent)
        } else if (mIsLeft) { //中間的 左滑
            mOffset = (mPercent + mPosition) * mDistance
            if (mPercent >= 0.5) {
                leftCircleX =
                    -(mCycleNumber - 1) * .5f * mDistance + ((mPercent - .5f) / .5f + mPosition) * mDistance
                rightCircleX = -(mCycleNumber - 1) * .5f * mDistance + (1 + mPosition) * mDistance
                rightCircleRadius =
                    minRadius + (maxRadius - minRadius) * (mPercent - .5f) / .5f
            } else {
                rightCircleX =
                    -(mCycleNumber - 1) * .5f * mDistance + (mPercent / .5f + mPosition) * mDistance
                leftCircleX = -(mCycleNumber - 1) * .5f * mDistance + mPosition * mDistance
                rightCircleRadius = minRadius
            }
            leftCircleRadius = mRadius * (1 - mPercent)
        } else { //右滑
            mOffset = (mPercent + mPosition) * mDistance
            if (mPercent <= 0.5) {
                leftCircleX = -(mCycleNumber - 1) * .5f * mDistance + mPosition * mDistance
                rightCircleX =
                    -(mCycleNumber - 1) * .5f * mDistance + (mPercent / .5f + mPosition) * mDistance
                leftCircleRadius =
                    minRadius + (maxRadius - minRadius) * (.5f - mPercent) / .5f
            } else {
                leftCircleX =
                    -(mCycleNumber - 1) * .5f * mDistance + ((mPercent - .5f) / .5f + mPosition) * mDistance
                rightCircleX = -(mCycleNumber - 1) * .5f * mDistance + (mPosition + 1) * mDistance
                leftCircleRadius = minRadius
            }
            rightCircleRadius = mRadius * mPercent
        }
        //右圓
        canvas.drawCircle(rightCircleX, 0f, rightCircleRadius, mFillPaint)
        //左圓
        canvas.drawCircle(leftCircleX, 0f, leftCircleRadius, mFillPaint)
        //貝塞爾
        //控製點
        mSpringPoint[0].x = leftCircleX
        mSpringPoint[0].y = -leftCircleRadius
        mSpringPoint[5].x = mSpringPoint[0].x
        mSpringPoint[5].y = leftCircleRadius
        //
        mSpringPoint[1].x = (leftCircleX + rightCircleX) / 2
        mSpringPoint[1].y = -leftCircleRadius / 2
        mSpringPoint[4].x = mSpringPoint[1].x
        mSpringPoint[4].y = leftCircleRadius / 2
        //
        mSpringPoint[2].x = rightCircleX
        mSpringPoint[2].y = -rightCircleRadius
        mSpringPoint[3].x = mSpringPoint[2].x
        mSpringPoint[3].y = rightCircleRadius
        mPath.reset()
        mPath.moveTo(mSpringPoint[0].x, mSpringPoint[0].y)
        mPath.quadTo(
            mSpringPoint[1].x,
            mSpringPoint[1].y,
            mSpringPoint[2].x,
            mSpringPoint[2].y
        )
        mPath.lineTo(mSpringPoint[3].x, mSpringPoint[3].y)
        mPath.quadTo(
            mSpringPoint[4].x,
            mSpringPoint[4].y,
            mSpringPoint[5].x,
            mSpringPoint[5].y
        )
        canvas.drawPath(mPath, mFillPaint)
    }

    /**
     * 繪製貝塞爾曲線
     *
     * @param canvas
     */
    private fun drawCubicBezier(canvas: Canvas) {
        //更換控製點
        changePoint()
        /** 清除Path中的內容
         * reset不保留內部數據結構，但會保留FillType.
         * rewind會保留內部的數據結構，但不保留FillType  */
        mPath.reset()

        //0
        mPath.moveTo(mControlPoint[0].x, mControlPoint[0].y)
        //0-3
        mPath.cubicTo(
            mControlPoint[1].x,
            mControlPoint[1].y,
            mControlPoint[2].x,
            mControlPoint[2].y,
            mControlPoint[3].x,
            mControlPoint[3].y
        )
        //3-6
        mPath.cubicTo(
            mControlPoint[4].x,
            mControlPoint[4].y,
            mControlPoint[5].x,
            mControlPoint[5].y,
            mControlPoint[6].x,
            mControlPoint[6].y
        )
        //6-9
        mPath.cubicTo(
            mControlPoint[7].x,
            mControlPoint[7].y,
            mControlPoint[8].x,
            mControlPoint[8].y,
            mControlPoint[9].x,
            mControlPoint[9].y
        )
        //9-0
        mPath.cubicTo(
            mControlPoint[10].x,
            mControlPoint[10].y,
            mControlPoint[11].x,
            mControlPoint[11].y,
            mControlPoint[0].x,
            mControlPoint[0].y
        )
        canvas.drawPath(mPath, mFillPaint)
    }

    /**
     * 控製點
     */
    private fun changePoint() {
        mCenterPoint.y = 0f
        var mc = M
        mControlPoint[2].y = mRadius //底部
        mControlPoint[8].y = -mRadius //頂部

        //圓心位置
        if (mPosition == mCycleNumber - 1 && !mIsLeft) { //第一個 右滑  0-->4
            if (mPercent <= 0.2) { //回彈 圓心到達
                mCenterPoint.x =
                    -(mCycleNumber - 1) * .5f * mDistance + (mCycleNumber - 1) * mDistance //最後一個
            } else if (mPercent <= 0.8) { //加速 左凸起 扁平化M 最右端固定不變  圓心移動
                mCenterPoint.x =
                    -(mCycleNumber - 1) * .5f * mDistance + (1 - (mPercent - .2f) / .6f) * (mCycleNumber - 1) * mDistance
            } else if (mPercent > 0.8 && mPercent < 1) { //
                mCenterPoint.x = -(mCycleNumber - 1) * .5f * mDistance //第一個
            } else if (mPercent == 1f) { //圓
                mCenterPoint.x = -(mCycleNumber - 1) * .5f * mDistance
            }
            //控製點位置
            if (mPercent > 0.8 && mPercent <= 1) { //右凸起 圓心不變
                mControlPoint[5].x =
                    mCenterPoint.x + mRadius * (2 - (mPercent - .8f) / .2f) //右半圓
                mControlPoint[0].x = mCenterPoint.x - mRadius //左半圓
            } else if (mPercent > 0.5 && mPercent <= 0.8) { //加速 左凸起 扁平化M 最右端固定不變  圓心移動
                mControlPoint[5].x = mCenterPoint.x + 2 * mRadius //右半圓
                mControlPoint[0].x =
                    mCenterPoint.x - mRadius * (1 + (.8f - mPercent) / .3f) //左半圓
                mControlPoint[2].y = mRadius * (1 + (mPercent - .8f) / .3f * .1f) //底部
                mControlPoint[8].y = -mRadius * (1 + (mPercent - .8f) / .3f * .1f) //頂部
                mc *= (1 + (-mPercent + .8f) / .3f * .3f)
            } else if (mPercent > 0.2 && mPercent <= 0.5) { //左右恢復 變圓M逐漸重置為原來大小  圓心移動
                mControlPoint[5].x =
                    mCenterPoint.x + mRadius * (1 + (mPercent - .2f) / .3f) //右半圓
                mControlPoint[0].x =
                    mCenterPoint.x - mRadius * (1 + (mPercent - .2f) / .3f) //左半圓
                mControlPoint[2].y = mRadius * (1 - (mPercent - .2f) / .3f * .1f) //底部
                mControlPoint[8].y = -mRadius * (1 - (mPercent - .2f) / .3f * .1f) //頂部
                mc *= (1 + (mPercent - .2f) / .3f * .3f)
            } else if (mPercent > 0.1 && mPercent <= 0.2) { //左凹 圓心到達.0
                mControlPoint[5].x = mCenterPoint.x + mRadius //右半圓
                mControlPoint[0].x =
                    mCenterPoint.x - mRadius * (1 - (.2f - mPercent) / .1f * .5f) //左半圓
            } else if (mPercent in 0.0..0.1) { //回彈 圓心到達
                mControlPoint[5].x = mCenterPoint.x + mRadius //右半圓
                mControlPoint[0].x = mCenterPoint.x - mRadius * (1 - mPercent / .1f * .5f) //左半圓
            }
        } else if (mPosition == mCycleNumber - 1 && mIsLeft) { //最後一個 左滑  4-->0
            if (mPercent <= 0.2) { //圓
                mCenterPoint.x =
                    -(mCycleNumber - 1) * .5f * mDistance + (mCycleNumber - 1) * mDistance
            } else if (mPercent <= 0.8) { //加速 左凸起 扁平化M 最右端固定不變  圓心移動
                mCenterPoint.x =
                    -(mCycleNumber - 1) * .5f * mDistance + (1 - (mPercent - .2f) / .6f) * (mCycleNumber - 1) * mDistance
            } else if (mPercent > 0.8 && mPercent < 1) { //回彈 圓心到達
                mCenterPoint.x = -(mCycleNumber - 1) * .5f * mDistance //第一個
            } else if (mPercent == 1f) { //圓
                mCenterPoint.x = -(mCycleNumber - 1) * .5f * mDistance + mPosition * mDistance
            }
            if (mPercent <= 0) { //圓
            } else if (mPercent in 0.0..0.2) { //左凸起 圓心不變
                mControlPoint[5].x = mCenterPoint.x + mRadius //右半圓
                mControlPoint[0].x = mCenterPoint.x - mRadius * (1 + mPercent / .2f) //左半圓
            } else if (mPercent > 0.2 && mPercent <= 0.5) { //加速 右凸起 扁平化M 最左端固定不變  圓心移動
                mControlPoint[5].x =
                    mCenterPoint.x + mRadius * (1 + (mPercent - .2f) / .3f) //右半圓
                mControlPoint[0].x = mCenterPoint.x - 2 * mRadius //左半圓
                mControlPoint[2].y = mRadius * (1 - (mPercent - .2f) / .3f * .1f) //底部
                mControlPoint[8].y = -mRadius * (1 - (mPercent - .2f) / .3f * .1f) //頂部
                mc *= (1 + (mPercent - .2f) / .3f * .3f)
            } else if (mPercent > 0.5 && mPercent <= 0.8) { //左右恢復 變圓M逐漸重置為原來大小  圓心移動
                mControlPoint[5].x =
                    mCenterPoint.x + mRadius * (1 + (.8f - mPercent) / .3f) //右半圓
                mControlPoint[0].x =
                    mCenterPoint.x - mRadius * (1 + (.8f - mPercent) / .3f) //左半圓
                mControlPoint[2].y = mRadius * (1 + (mPercent - .8f) / .3f * .1f) //底部
                mControlPoint[8].y = -mRadius * (1 + (mPercent - .8f) / .3f * .1f) //頂部
                mc *= (1 + (.8f - mPercent) / .3f * .3f)
            } else if (mPercent > 0.8 && mPercent <= 0.9) { //右凹 圓心到達
                mControlPoint[5].x =
                    mCenterPoint.x + mRadius * (1 - (mPercent - .8f) / .1f * .5f) //右半圓
                mControlPoint[0].x = mCenterPoint.x - mRadius //左半圓
            } else if (mPercent > 0.9 && mPercent <= 1) { //回彈 圓心到達
                mControlPoint[5].x =
                    mCenterPoint.x + mRadius * (1 - (mPercent - .9f) / .1f * .5f) //右半圓
                mControlPoint[0].x = mCenterPoint.x - mRadius //左半圓
            }
        } else {
            if (mPercent <= 0.2) { //圓
                mCenterPoint.x = -(mCycleNumber - 1) * .5f * mDistance + mPosition * mDistance
            } else if (mPercent <= 0.8) { //加速 左凸起 扁平化M 最右端固定不變  圓心移動
                mCenterPoint.x =
                    -(mCycleNumber - 1) * .5f * mDistance + (mPosition + mPercent) * mDistance
                mCenterPoint.x =
                    -(mCycleNumber - 1) * .5f * mDistance + (mPosition + (mPercent - .2f) / .6f) * mDistance
            } else if (mPercent > 0.8 && mPercent < 1) { //回彈 圓心到達
                mCenterPoint.x =
                    -(mCycleNumber - 1) * .5f * mDistance + (mPosition + 1) * mDistance
            } else if (mPercent == 1f) { //圓
                mCenterPoint.x = -(mCycleNumber - 1) * .5f * mDistance + mPosition * mDistance
            }
            //控製點位置
            if (mIsLeft) { //左滑
                if (mPercent in 0.0..0.2) { //右凸起 圓心不變
                    mControlPoint[5].x =
                        mCenterPoint.x + mRadius * (2 - (.2f - mPercent) / .2f) //右半圓
                    mControlPoint[0].x = mCenterPoint.x - mRadius //左半圓
                } else if (mPercent > 0.2 && mPercent <= 0.5) { //加速 左凸起 扁平化M 最右端固定不變  圓心移動
                    mControlPoint[5].x = mCenterPoint.x + 2 * mRadius //右半圓
                    mControlPoint[0].x =
                        mCenterPoint.x - mRadius * (1 + (mPercent - .2f) / .3f) //左半圓
                    mControlPoint[2].y = mRadius * (1 - (mPercent - .2f) / .3f * .1f) //底部
                    mControlPoint[8].y = -mRadius * (1 - (mPercent - .2f) / .3f * .1f) //頂部
                    mc *= (1 + (mPercent - .2f) / .3f * .3f)
                } else if (mPercent > 0.5 && mPercent <= 0.8) { //左右恢復 變圓M逐漸重置為原來大小  圓心移動
                    mControlPoint[5].x =
                        mCenterPoint.x + mRadius * (1 + (.8f - mPercent) / .3f) //右半圓
                    mControlPoint[0].x =
                        mCenterPoint.x - mRadius * (1 + (.8f - mPercent) / .3f) //左半圓
                    mControlPoint[2].y = mRadius * (1 + (mPercent - .8f) / .3f * .1f) //底部
                    mControlPoint[8].y = -mRadius * (1 + (mPercent - .8f) / .3f * .1f) //頂部
                    mc *= (1 + (-mPercent + .8f) / .3f * .3f)
                } else if (mPercent > 0.8 && mPercent <= 0.9) { //左凹 圓心到達
                    mControlPoint[5].x = mCenterPoint.x + mRadius //右半圓
                    mControlPoint[0].x =
                        mCenterPoint.x - mRadius * (1 - (mPercent - .8f) / .1f * .5f) //左半圓
                } else if (mPercent > 0.9 && mPercent <= 1) { //回彈 圓心到達
                    mControlPoint[5].x = mCenterPoint.x + mRadius //右半圓
                    mControlPoint[0].x =
                        mCenterPoint.x - mRadius * (1 - (1f - mPercent) / .1f * .5f) //左半圓
                }
            } else { //右滑
                if (mPercent in 0.8..1.0) { //左凸起 圓心不變
                    mControlPoint[5].x = mCenterPoint.x + mRadius //右半圓
                    mControlPoint[0].x =
                        mCenterPoint.x - mRadius * (2 - (mPercent - .8f) / .2f) //左半圓
                } else if (mPercent > 0.5 && mPercent <= 0.8) { //加速 右凸起 扁平化M 最左端固定不變  圓心移動
                    mControlPoint[5].x =
                        mCenterPoint.x + mRadius * (2 - (mPercent - .5f) / .3f) //右半圓
                    mControlPoint[0].x = mCenterPoint.x - 2 * mRadius //左半圓
                    mControlPoint[2].y = mRadius * (1 - (.8f - mPercent) / .3f * .1f) //底部
                    mControlPoint[8].y = -mRadius * (1 - (.8f - mPercent) / .3f * .1f) //頂部
                    mc *= (1 + (.8f - mPercent) / .3f * .3f)
                } else if (mPercent > 0.2 && mPercent <= 0.5) { //左右恢復 變圓M逐漸重置為原來大小  圓心移動
                    mControlPoint[5].x =
                        mCenterPoint.x + mRadius * (1 + (mPercent - .2f) / .3f) //右半圓
                    mControlPoint[0].x =
                        mCenterPoint.x - mRadius * (1 + (mPercent - .2f) / .3f) //左半圓
                    mControlPoint[2].y = mRadius * (1 - (mPercent - .2f) / .3f * .1f) //底部
                    mControlPoint[8].y = -mRadius * (1 - (mPercent - .2f) / .3f * .1f) //頂部
                    mc *= (1 + (mPercent - .2f) / .3f * .3f)
                } else if (mPercent > 0.1 && mPercent <= 0.2) { //右凹 圓心到達
                    mControlPoint[5].x =
                        mCenterPoint.x + mRadius * (1 - (.2f - mPercent) / .1f * .5f) //右半圓
                    mControlPoint[0].x = mCenterPoint.x - mRadius //左半圓
                } else if (mPercent in 0.0..0.1) { //回彈 圓心到達
                    mControlPoint[5].x =
                        mCenterPoint.x + mRadius * (1 - mPercent / .1f * .5f) //右半圓
                    mControlPoint[0].x = mCenterPoint.x - mRadius //左半圓
                }
            }
        }

        //11 0 1
        mControlPoint[0].y = 0f
        mControlPoint[1].x = mControlPoint[0].x
        mControlPoint[1].y = mRadius * mc
        mControlPoint[11].x = mControlPoint[0].x
        mControlPoint[11].y = -mRadius * mc
        //2 3 4
        mControlPoint[2].x = mCenterPoint.x - mRadius * mc
        mControlPoint[3].x = mCenterPoint.x
        mControlPoint[3].y = mControlPoint[2].y
        mControlPoint[4].x = mCenterPoint.x + mRadius * mc
        mControlPoint[4].y = mControlPoint[2].y
        //5 6 7
        mControlPoint[5].y = mRadius * mc
        mControlPoint[6].x = mControlPoint[5].x
        mControlPoint[6].y = 0f
        mControlPoint[7].x = mControlPoint[5].x
        mControlPoint[7].y = -mRadius * mc
        //8 9 10
        mControlPoint[8].x = mCenterPoint.x + mRadius * mc
        mControlPoint[9].x = mCenterPoint.x
        mControlPoint[9].y = mControlPoint[8].y
        mControlPoint[10].x = mCenterPoint.x - mRadius * mc
        mControlPoint[10].y = mControlPoint[8].y
    }

    private val mControlPoint by lazy {
        arrayOf(Point(), Point(), Point(), Point(), Point(), Point(), Point(), Point(), Point(), Point(), Point(), Point())
    }

    private val mCenterPoint = CenterPoint()

    internal inner class CenterPoint {
        var x = 0f
        var y = 0f
    }

    internal inner class Point {
        var x = 0f
        var y = 0f
    }

    /**
     * 移動指示點
     *
     * @param percent  比例
     * @param position 第幾個
     * @param isLeft   是否左滑
     */
    private fun move(percent: Float, position: Int, isLeft: Boolean) {
        mPosition = position
        mPercent = percent
        mIsLeft = isLeft
        when (mIndicatorType) {
            IndicatorType.CIRCLE_LINE -> {
                if (mPosition == mCycleNumber - 1 && !isLeft) { //第一個 右滑
                    mOffset = percent * mDistance
                }
                mOffset = if (mPosition == mCycleNumber - 1 && isLeft) { //最後一個 左滑
                    percent * mDistance
                } else { //中間
                    percent * mDistance
                }
            }
            IndicatorType.CIRCLE, IndicatorType.LINE, IndicatorType.PROGRESS -> mOffset =
                if (mPosition == mCycleNumber - 1 && !isLeft) { //第一個 右滑
                    (1 - percent) * (mCycleNumber - 1) * mDistance
                } else if (mPosition == mCycleNumber - 1 && isLeft) { //最後一個 左滑
                    (1 - percent) * (mCycleNumber - 1) * mDistance
                } else { //中間的
                    (percent + mPosition) * mDistance
                }
            else -> {
            }
        }
        invalidate()
    }

    /**
     * 個數
     *
     * @param num
     */
    fun setCycleNumber(num: Int) = apply {
        mCycleNumber = num
        invalidate()
    }

    /**
     * 類型
     *
     * @param indicatorType
     */
    fun setType(indicatorType: IndicatorType) = apply {
        mIndicatorType = indicatorType
        invalidate()
    }

    /**
     * 半徑
     *
     * @param radius
     */
    fun setRadius(radius: Float) = apply {
        mRadius = radius
        invalidate()
    }

    /**
     * 距離在IndicatorDistanceType為BYDISTANCE下作用
     *
     * @param distance
     */
    fun setDistance(distance: Float) = apply {
        mDistance = distance
        invalidate()
    }

    /**
     * 距離類型
     *
     * @param distanceType
     */
    fun setDistanceType(distanceType: DistanceType) = apply {
        this.mDistanceType = distanceType
        invalidate()
    }

    fun setViewPager(viewpager: ViewPager2, cycleNumber: Int) = apply {
        mCycleNumber = cycleNumber
        viewpager.registerOnPageChangeCallback(object : OnPageChangeCallback() {
            //記錄上一次滑動的positionOffsetPixels值
            private var lastValue = -1
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                if (!mAnimation) {
                    //不需要動畫
                    return
                }
                var isLeft = mIsLeft
                if (lastValue / 10 > positionOffsetPixels / 10) {
                    //右滑
                    isLeft = false
                } else if (lastValue / 10 < positionOffsetPixels / 10) {
                    //左滑
                    isLeft = true
                }
                if (mCycleNumber > 0) {
                    move(positionOffset, position % mCycleNumber, isLeft)
                }
                lastValue = positionOffsetPixels
            }

            override fun onPageSelected(position: Int) {
                if (mAnimation) {
                    //需要動畫
                    return
                }
                if (mCycleNumber > 0) {
                    move(0f, position % mCycleNumber, false)
                }
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })
    }
}